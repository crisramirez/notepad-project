<div class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <span class="navbar-brand">
                <?php if(isset($user['name'])): ?>
                    <i class="text-small"><?= ucwords($user['name']) ?>'s</i>
                <?php endif; ?>
                <a href="<?= ROOT ?>">Notepad</a>
            </span>
        </div>

        <div class="collapse navbar-collapse" id="main-menu">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?= ROOT ?>/new.php"><i class="fa fa-plus"></i></a></li>
                <li><a href="<?= ROOT ?>/accounts/logout.php"><i class="fa fa-sign-out"></i></a></li>
            </ul>
        </div>
    </div>
</div>