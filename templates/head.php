<?php
/**
 * This file will:
 * 1) Set meta data (title and description)
 * 2) Load our javascript and css files
 */

//if title is not set then use a default value
$title = isset($title) ? $title : 'Notepad by CR7';

/**
 * --ASK IF YOU DONT UNDERSTAND--
 * version for the css and javascript files
 * after a while unless the url for js and css is different, the browser will save them and even if you make a change
 * it wont be reflected. Adding this string at the end ensures that it will load a new files at least every minute
 */
$v = date('YmdHi');
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $title ?></title>
    <!-- bootstrap, font awesome and jquery-ui css -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">

    <!-- custom css files -->
    <link href="<?= CDN ?>/css/presets.css?v=<?= $v ?>" rel="stylesheet">
    <link href="<?= CDN ?>/css/style.css?v=<?= $v ?>" rel="stylesheet">

    <!-- bootstrap, jquery and jquery-ui js -->
    <script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- custom javascript -->
    <script src="<?= CDN ?>/js/main.js?v=<?= $v ?>"></script>
</head>