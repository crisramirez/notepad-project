<?php

include_once '../functions.php';
include_once '../config.php';

//if no id is set then redirect to index
if (!isset($_GET['id'])) {
    header("Location: index.php");
}

$conn = dbConnect(); //custom function
$id = $_GET['id'];
$cond = isset($_POST['title']) && isset($_POST['content']);
if ($cond) {
    //add user_id = id so that you only update a note that belongs to you
    $sql = "UPDATE `notes` SET `title`='{$_POST['title']}', `content`='{$_POST['content']}' WHERE `id`=$id AND `user_id`={$user['id']}";
    $result = mysqli_query($conn, $sql);
    if (mysqli_error($conn)) {
        error(mysqli_error($conn)); //custom function
    }

    header('Location: index.php?save=success');
    exit();
}

$sql = "SELECT * FROM `notes` WHERE `id` = $id"; //not correct way to do it
//you can get hacked - look into SQL injections in php

$result = mysqli_query($conn, $sql);
if (mysqli_error($conn)) {
    error(mysqli_error($conn)); //custom function
}

$note = mysqli_fetch_assoc($result);
?>


<!DOCTYPE html>
<html>
<head>
    <?php
    include_once '../templates/head.php';
    ?>
</head>
<body>

<?php
include_once '../templates/header.php';
?>
<!-- use container not container-fluid -->
<!-- container restricts the width to 1020px, container-fluid will go all the way which in this case you don't want -->
<div class="container">

    <div class="well">
        <h1>New Note</h1>
        <form action="" method="post">
            <!-- <br> should be used only inside of <p> tags -->
            <!-- to stack things together use <div> and apply proper margin in your css for the <div> -->
            <!-- in this case the margin will be done by .form-group and .form-control -->
            <!-- there should be a form-group per field not one for the whole form -->
            <div class="form-group">
                <label>Title:</label>
                <input name="title" type="text" class="form-control" value="<?= $note['title'] ?>">
            </div>
            <div class="form-group">
                <!-- the no-resize class will restrict the user from resizing the textarea -->
                <!-- this will prevent the UI from breaking -->
                <textarea placeholder="What's on your mind..." name="content" class="form-control no-resize" rows="10"><?= $note['content'] ?></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
            <!-- buttons are by defaut type submit so set it to type="button" -->
            <!-- otherwise the form will submit when you hit cancel -->
            <button type="button" onclick="window.location = 'index.php'" class="btn btn-danger"> Cancel</button>
        </form>
    </div>
</div>

<?php
include_once '../templates/footer.php';
?>
</body>
</html>
