<?php
/**
 * because this is wrapped in a false it wont run
 * I just wanted you to see what you had before
 *
 * You don't even display anything in this page and you still have HTML
 * that's not necessary
 */

if (false): ?>
<?php
session_start();
?>
<head>
    
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
    <?php
    $conn = mysqli_connect('localhost', 'root', '', 'notepad');
    $id = $_GET['id'];
    $sql = "DELETE FROM notes WHERE id = $id";
    $result = mysqli_query($conn, $sql);
    header('Location: index.php?');
    exit();
    ?>
</body>
<?php else: ?>

    <!-- ACTUAL CODE HERE -->
    <?php
    include_once '../functions.php';
    include_once '../config.php';

    $conn = dbConnect(); //custom function
    $id = $_GET['id'];
    //add user_id = id so that you only delete a note that belongs to you
    $sql = "DELETE FROM `notes` WHERE `id` = $id AND `user_id`={$user['id']}"; //again, not-good look into SQL injections
    $result = mysqli_query($conn, $sql);
    if (mysqli_error($conn)) {
        error(mysqli_error($conn)); //custom function
    }
    header('Location: index.php?delete=success');
    exit();
    ?>

<?php endif; ?>
