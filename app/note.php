<?php

include_once '../functions.php';
include_once '../config.php';


$conn = dbConnect(); //custom function
$id = $_GET['id'];

//add user_id = id so that you only get a note that belongs to you
$sql = "SELECT * FROM `notes` WHERE `id` = $id AND `user_id`={$user['id']}"; //not correct way to do it
//you can get hacked - look into SQL injections in php

$result = mysqli_query($conn, $sql);
$note = mysqli_fetch_assoc($result);

$sort = 'date';
$order = 'desc';
if (isset($_GET['sort'])) {
    $sort = $_GET['sort'];
}
if (isset($_GET['order'])) {
    $order = $_GET['order'];
}
$sql1 = "SELECT * FROM `notes` WHERE `user_id`={$user['id']} ORDER BY $sort $order";
$result1 = mysqli_query($conn, $sql);
if (mysqli_error($conn)) {
    error(mysqli_error($conn)); //custom function
}
?>


<!DOCTYPE html>
<html>
<head>
    <?php
    include_once '../templates/head.php';
    ?>
</head>
<body>

<?php
include_once '../templates/header.php';
?>
<!-- use container not container-fluid -->
<!-- container restricts the width to 1020px, container-fluid will go all the way which in this case you don't want -->
<div class="container">
    <?php while ($row = mysqli_fetch_assoc($result1)):?>
    <h1><?php echo $note['title'] ?></h1>
    <!-- add bottom class will add 15px to the bottom of the div to give proper spacing -->
    <div class="text-muted">
                    <?php
                    $date = date_create($row['date']);
                    ?>
                    Created: <i><?= date_format($date, 'l jS \o\f F Y h:i A'); ?></i>
    </div>
    <div class="btn-group add-bottom">
        <button onclick="window.location = 'delete.php?id=<?php echo $id ?>'" class="btn btn-danger">
            Delete
        </button>
        <button onclick="window:location = 'update.php?id=<?php echo $id ?>'" class="btn btn-primary">
            Update
        </button>
    </div>

    <!-- nl2br() function is built in to php-->
    <!-- it will turn \n characters that would normally be treated as a space into a '<br>' tag -->
    <p><?php echo nl2br($note['content']) ?></p>
       <?php endwhile; ?>
</div>

<?php
include_once '../templates/footer.php';
?>
</body>
</html>