<?php
include_once '../functions.php';
include_once '../config.php';


    if (count($_POST) > 0) {
        if (isset($_POST['title']) && !empty($_POST['title'])) {
            $title = $_POST['title'];
        } else {
            $title = substr($_POST['content'], 0, 25);
        }
        if (isset($_POST['content']) && !empty($_POST['content'])){
            $content = $_POST['content'];
        
        $conn = dbConnect(); //custom function
       
        $sql = "INSERT INTO `notes` (`user_id`, `title`, `content`) VALUES('{$user['id']}', '{$title}', '{$content}')";
        
        if (mysqli_query($conn, $sql)) {
            //redirect to index
            header('Location: index.php?save=success');
            exit();
        } else {
            errorMsg('Error: ' . mysqli_error($conn));
        }
    }else{
        $error = "content required";
    }
}
?>


<!DOCTYPE html>
<html>
    <head>
        <?php
        include_once '../templates/head.php';
        ?>
    </head>
    <body>

        <?php
        include_once '../templates/header.php';
        ?>
        <!-- use container not container-fluid -->
        <!-- container restricts the width to 1020px, container-fluid will go all the way which in this case you don't want -->
        <div class="container">
            <!-- This is your old code
            **compare it to my code**
            <form action="" method="post">
                <div class="form-group">
                    Title: <input name="title" type="text" class="form-control"> <br> <br>
                    Text: <textarea placeholder="type here... " name="content" class="form-control" rows="10"></textarea>
                    <br>
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button onclick="window.location = 'index.php'" class="btn btn-danger"> Cancel</button>
                </div>
            </form>
            -->
         <?php if (isset($error)){
         echo $error; } ?>
          
            <div class="well">
                <h1>New Note</h1>
                <form action="" method="post">
                    <!-- <br> should be used only inside of <p> tags -->
                    <!-- to stack things together use <div> and apply proper margin in your css for the <div> -->
                    <!-- in this case the margin will be done by .form-group and .form-control -->
                    <!-- there should be a form-group per field not one for the whole form -->
                    <div class="form-group">
                        <label>Title:</label>
                        <input name="title" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <!-- the no-resize class will restrict the user from resizing the textarea -->
                        <!-- this will prevent the UI from breaking -->
                        <textarea placeholder="What's on your mind..." name="content" class="form-control no-resize" rows="10"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                    <!-- buttons are by defaut type submit so set it to type="button" -->
                    <!-- otherwise the form will submit when you hit cancel -->
                    <button type="button" onclick="window.location = 'index.php'" class="btn btn-danger"> Cancel</button>
                </form>
            </div>
        </div>

        <?php
        include_once '../templates/footer.php';
        ?>
    </body>
</html>
