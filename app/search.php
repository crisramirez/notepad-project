<?php

include_once '../functions.php';
include_once '../config.php';

//you were using $_POST for this, use $_GET
//$_POST is to create new data
//$_GET is to get data from the server
if (isset($_GET['search'])) {
    $conn = dbConnect();

    $search = $_GET['search'];

    $sql = "SELECT * FROM `notes` WHERE `title` LIKE '%$search%' AND `user_id`={$user['id']}";
    $result = mysqli_query($conn, $sql);
    if (mysqli_error($conn)) {
        error(mysqli_error($conn)); //custom function
    }
    $count = mysqli_num_rows($result);
} else {
    $count = 0;
    $search = '';
}
?>


<!DOCTYPE html>
<html>
<head>
    <?php
    include_once '../templates/head.php';
    ?>
</head>
<body>

<?php
include_once '../templates/header.php';
?>
<!-- use container not container-fluid -->
<!-- container restricts the width to 1020px, container-fluid will go all the way which in this case you don't want -->
<div class="container">
    <!-- no-padding class get rid of the padding so that the form is inline with everything else -->
    <!-- try to remove the class so you see the difference -->
    <form action="search.php" method="get" class="navbar-form no-padding" role="search">
        <div class="form-group">
            <input type="text" name="search" placeholder="Search your notes" class="form-control" value="<?= $search ?>">
        </div>
        <input type="submit" name="submit" value="search" class="btn btn-default">
    </form>


    <div id="notes">
        <?php if ($count == 0): ?>
        No results found
        <?php else: ?>
        <?php while ($row = mysqli_fetch_assoc($result)): ?>
            <!-- the class note-row will add borders and proper padding/margin for each note -->
            <div class="note-row">
                <!-- text larger makes... the text larger -->
                <div class="text-larger">
                    <a href="note.php?id=<?php echo $row['id'] ?>"><b><?php echo $row['title'] ?></b></a>
                </div>
                <!-- text-muted makes the text lighter color -->
                <!-- The date is a secondary element therefore you want it to be subtle -->
                <div class="text-muted">
                    <?php
                    $date = date_create($row['date']);
                    ?>
                    Created: <i><?= date_format($date, 'l jS \o\f F Y h:i A'); ?></i>
                </div>
            </div>
        <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div>

<?php
include_once '../templates/footer.php';
?>
</body>
</html>