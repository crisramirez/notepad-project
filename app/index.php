<?php

include_once '../functions.php';
include_once '../config.php';

$conn = dbConnect();

$sort = 'date';
$order = 'desc';
if (isset($_GET['sort'])) {
    $sort = $_GET['sort'];
}
if (isset($_GET['order'])) {
    $order = $_GET['order'];
}

$sql = "SELECT * FROM `notes` WHERE `user_id`={$user['id']} ORDER BY $sort $order";
$result = mysqli_query($conn, $sql);
if (mysqli_error($conn)) {
    error(mysqli_error($conn)); //custom function
}
?>


<!DOCTYPE html>
<html>
<head>
    <?php
    include_once '../templates/head.php';
    ?>
</head>
<body>

<?php
include_once '../templates/header.php';
?>
<!-- use container not container-fluid -->
<!-- container restricts the width to 1020px, container-fluid will go all the way which in this case you don't want -->
<div class="container">
    <!-- no-padding class get rid of the padding so that the form is inline with everything else -->
    <!-- try to remove the class so you see the difference -->
    <form action="search.php" method="get" class="navbar-form no-padding" role="search">
        <div class="form-group">
            <input type="text" name="search" placeholder="Search your notes" class="form-control">
        </div>
        <input type="submit" name="submit" value="search" class="btn btn-default">
    </form>

    <div>
        <!-- inline-block class turns an element into an inline element -->
        <!-- without it, the two blocks would be stacked not side by side -->
        <?php if (mysqli_num_rows($result) < 1){
            echo "No notes yet. "; ?>
        <a href="<?= ROOT ?>/new.php"> Add a new note. </a>
        <?php }else{ ?>
        <div class="inline-block">
            Sort by title: <a href="index.php?sort=title&order=asc"> ASC </a> |
            <a href="index.php?sort=title&order=desc"> DESC </a>
        </div>
        <div class="inline-block">
            Sort by date: <a href="index.php?sort=date&order=asc"> ASC</a> |
            <a href="index.php?sort=date&order=desc"> DESC </a>
        </div>
    </div>

    <!--
    I want you to take a look at the mess you have here
    <blockquote>                                                 ** why blockquote? always use <div> for generic things
        <p>
            <?php //while ($row = mysqli_fetch_assoc($result)): ?> ** you opened the while look in the middle of a <p> tag
                                                                 ** basically every you will end up with one <p> and multiple </p>
            <a href="note.php?id=<?php //echo $row['id'] ?>"><b><?php echo $row['title'] ?></b></a>
        </p>
        <footer>                                                 ** footer is for the bottom of the page not for the bottom
                                                                 ** of each element
            <?php
    //$date = date_create($row['date']);
    // this should be 'l jS \o\f F Y h:i A' otherwise 'f' wont be escaped
    //echo date_format($date, 'l jS \of F Y h:i A');
    ?>
        </footer>
        <?php //endwhile; ?>
    </blockquote>

    Overall your html is pretty bad.
        -use <p> for paragraphs only
        -use <blockquote> for quotes only
        -use <footer> for the footer only
    For generic divisions in your HTML use <div> for blocks and <span> for inline
    you used <p> to wrap the title link... that's not a paragraph
    you used blockquote to wrap everything... EVERYTHING is not a quote
    you used footer to wrap the date... that's not a footer
    -->

    <div id="notes">
        <?php while ($row = mysqli_fetch_assoc($result)): ?>
            <!-- the class note-row will add borders and proper padding/margin for each note -->
            <div class="note-row">
                <!-- text larger makes... the text larger -->
                <div class="text-larger">
                    <a href="note.php?id=<?php echo $row['id'] ?>"><b><?php echo $row['title'] ?></b></a>
                </div>
                <!-- text-muted makes the text lighter color -->
                <!-- The date is a secondary element therefore you want it to be subtle -->
                <div class="text-muted">
                    <?php
                    $date = date_create($row['date']);
                    ?>
                    Created: <i><?= date_format($date, 'l jS \o\f F Y h:i A'); ?></i>
                </div>
            </div>
        
        <?php endwhile; ?>
        <?php } ?>    
    </div>
</div>

<?php
include_once '../templates/footer.php';
?>
</body>
</html>